#!/bin/bash

NEWLOC=`curl -L "https://sourceforge.net/projects/jedit/files/latest/download" -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36" 2>/dev/null | grep -i .dmg | head -1 | awk -F 'url=' '{print $2}' | sed 's@">@@g'`


if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi